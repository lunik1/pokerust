{
  description = "Rust wrapper for PokéAPI";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk.url = "github:nix-community/naersk";
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    pre-commit-hooks = {
      url = "github:cachix/pre-commit-hooks.nix";
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
  };

  outputs =
    inputs@{ self, ... }:
    with inputs;
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = (import nixpkgs) { inherit system overlays; };
        naersk' = pkgs.callPackage naersk { };
      in
      {
        packages = rec {
          default = naersk'.buildPackage {
            src = ./.;
            strictDeps = true;
            doCheck = true;
          };
          pokerust = default;
        };

        devShells.default = pkgs.mkShell {
          inherit (self.checks.${system}.pre-commit-check) shellHook;
          nativeBuildInputs = with pkgs; [
            # Rust
            (rust-bin.stable.latest.default.override {
              extensions = [
                "rust-analyzer"
                "rust-src"
              ];
            })
            cargo-audit
            cargo-outdated

            # Nix
            nixfmt-rfc-style
            nil
            statix

            # Toml
            taplo
          ];
        };

        formatter = pkgs.nixpgks-fmt;

        checks = {
          pre-commit-check = pre-commit-hooks.lib.${system}.run {
            src = ./.;
            hooks = {
              nixfmt-rfc-style.enable = true;
              rustfmt.enable = true;
              taplo.enable = true;
              copier-rejects = {
                enable = true;
                entry = "found Copier update rejection files; review them and remove them";
                files = "\\.rej$";
                language = "fail";
              };
            };
          };
        };
      }
    );
}
