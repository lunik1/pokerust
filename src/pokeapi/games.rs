use serde::{Deserialize, Serialize};

use super::locations::Region;
use super::moves::{Move, MoveLearnMethod};
use super::pokemon::{Ability, PokemonSpecies, Type};
use super::resource_lists::NamedAPIResourceList;
use super::utility::{Description, Name, NamedAPIResource};

use crate::{impl_id_and_named, set_endpoint};

/// <https://pokeapi.co/docs/v2#generations>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct Generation {
    pub id: i16,
    pub name: String,
    pub abilities: Vec<NamedAPIResource<Ability>>,
    pub names: Vec<Name>,
    pub main_region: NamedAPIResource<Region>,
    pub moves: Vec<NamedAPIResource<Move>>,
    pub pokemon_species: Vec<NamedAPIResource<PokemonSpecies>>,
    pub types: Vec<NamedAPIResource<Type>>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

/// <https://pokeapi.co/docs/v2#pokedexes>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct Pokedex {
    pub id: i16,
    pub name: String,
    pub is_main_series: bool,
    pub descriptions: Vec<Description>,
    pub names: Vec<Name>,
    pub pokemon_entries: Vec<PokemonEntry>,
    pub region: Option<NamedAPIResource<Region>>,
    pub version_groups: Vec<NamedAPIResource<VersionGroup>>,
}

/// <https://pokeapi.co/docs/v2#pokemonentry>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct PokemonEntry {
    pub entry_number: u16,
    pub pokemon_species: NamedAPIResource<PokemonSpecies>,
}

/// <https://pokeapi.co/docs/v2#version>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct Version {
    pub id: i16,
    pub name: String,
    pub names: Vec<Name>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

/// <https://pokeapi.co/docs/v2#version-groups>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct VersionGroup {
    pub id: i16,
    pub name: String,
    pub order: u16,
    pub generation: NamedAPIResource<Generation>,
    pub move_learn_methods: Vec<NamedAPIResource<MoveLearnMethod>>,
    pub pokedexes: Vec<NamedAPIResource<Pokedex>>,
    pub regions: Vec<NamedAPIResource<Region>>,
    pub versions: Vec<NamedAPIResource<Version>>,
}

set_endpoint!(Generation, NamedAPIResourceList, "generation");
set_endpoint!(Pokedex, NamedAPIResourceList, "pokedex");
set_endpoint!(Version, NamedAPIResourceList, "version");
set_endpoint!(VersionGroup, NamedAPIResourceList, "version-group");

impl_id_and_named!(Generation);
impl_id_and_named!(Pokedex);
impl_id_and_named!(Version);
impl_id_and_named!(VersionGroup);
