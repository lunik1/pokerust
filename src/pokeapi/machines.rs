use serde::{Deserialize, Serialize};

use super::games::VersionGroup;
use super::items::Item;
use super::moves::Move;
use super::resource_lists::APIResourceList;
use super::utility::NamedAPIResource;

use crate::{impl_id, set_endpoint};

/// <https://pokeapi.co/docs/v2#machines>
#[derive(Serialize, Deserialize, Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "deny_unknown_fields", serde(deny_unknown_fields))]
#[non_exhaustive]
pub struct Machine {
    pub id: i16,
    pub item: NamedAPIResource<Item>,
    #[serde(rename = "move")]
    pub move_: NamedAPIResource<Move>,
    pub version_group: NamedAPIResource<VersionGroup>,
}

set_endpoint!(Machine, APIResourceList, "machine");

impl_id!(Machine);
