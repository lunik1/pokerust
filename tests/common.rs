pub fn make_mock(loc: &str, json: &str) -> (mockito::ServerGuard, mockito::Mock) {
    let mut server = mockito::Server::new();
    let mock = server
        .mock("GET", loc)
        .with_status(200)
        .with_header("content-type", "application/json; charset=utf-8")
        .with_body(json)
        .create();
    (server, mock)
}
