#![expect(clippy::significant_drop_tightening)]

use cached::cached_key_result;
use cached::UnboundCache;
use minreq::Response;
use std::env;

fn get_endpoint() -> String {
    match env::var("POKERUST_ENDPOINT") {
        Ok(val) => val,
        Err(env::VarError::NotPresent) => String::from("https://pokeapi.co/api/v2/"),
        Err(env::VarError::NotUnicode(_)) => panic!(("POKERUST_ENDPOINT was not valid unicode.")),
    }
}

cached_key_result! {
   POKEAPI_CACHE: UnboundCache<(String, String), Response> = UnboundCache::new();
   Key = { (get_endpoint(), path.to_owned()) };
   fn get_resource(path: &str) -> Result<Response, minreq::Error> = {
       minreq::get(format!("{}{}", get_endpoint(), path)).send()
   }
}
